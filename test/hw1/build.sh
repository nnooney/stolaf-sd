#!/bin/bash -x

# This script is meant to be run from the main project folder, not the current
# folder it is in. Run it like this:
#  ./test/hw1/build.sh

mkdir build
mkdir build/hw1

g++ -o build/hw1/hello hw1/hello.cpp
g++ -o build/hw1/first hw1/first.cpp
g++ -o build/hw1/second hw1/second.cpp
g++ -o build/hw1/secondComment hw1/secondComment.cpp

# Build the Dog program using the Makefile in hw1, but move the relevant files
# to the build directory for testing.
(cd hw1 && make tryDog)
# Normally the .o and executable files will be made directly inside the hw1
# directory, but we want to move them to the build directory
mv hw1/*.o build/hw1
mv hw1/tryDog build/hw1

if [ "$0" = "$BASH_SOURCE" ]; then
  exit 0
else
  return 0
fi
