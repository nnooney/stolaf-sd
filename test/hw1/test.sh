#!/bin/bash

# This script is meant to be run from the main project folder, not the current
# folder it is in. Run it like this:
#  ./test/hw1/test.sh

TESTS=0
NUM_TESTS=5
ASSIGNMENT="hw1"

# Run the following tests, keeping track of their output. For each test, set
# the required variables, then execute the 'run and compare output test'.

PROGRAM_FILE="hello.cpp"
PROGRAM="build/hw1/hello"
EXPECTED_OUTPUT="Hello, world!"
. test/utils/runCompareOutput-test.sh
TESTS=$(($TESTS + $?))

PROGRAM_FILE="first.cpp"
PROGRAM="build/hw1/first"
EXPECTED_OUTPUT="Hello World!"
. test/utils/runCompareOutput-test.sh
TESTS=$(($TESTS + $?))

PROGRAM_FILE="second.cpp"
PROGRAM="build/hw1/second"
EXPECTED_OUTPUT="Hello World! I'm a C++ program"
. test/utils/runCompareOutput-test.sh
TESTS=$(($TESTS + $?))

PROGRAM_FILE="secondComment.cpp"
PROGRAM="build/hw1/secondComment"
EXPECTED_OUTPUT="Hello World! I'm a C++ program"
. test/utils/runCompareOutput-test.sh
TESTS=$(($TESTS + $?))

PROGRAM_FILE="tryDog.cpp"
PROGRAM="build/hw1/tryDog"
EXPECTED_OUTPUT="Hello, world!
My dog is named Fido, and it is 3 years old."
. test/utils/runCompareOutput-test.sh
TESTS=$(($TESTS + $?))

# All testing is complete
PASSED_TESTS=`expr ${NUM_TESTS} - ${TESTS}`
echo "Test Results: ${PASSED_TESTS}/${NUM_TESTS}"

if [ "$0" = "$BASH_SOURCE" ]; then
  exit $TESTS
else
  return $TESTS
fi
