#!/bin/bash -x

# This script is meant to be run from the main project folder, not the current
# folder it is in. Run it like this:
#  ./test/hw2/build.sh
ASSIGNMENT="hw2"

mkdir build
mkdir build/${ASSIGNMENT}

g++ -o build/${ASSIGNMENT}/types ${ASSIGNMENT}/types.cpp

if [ "$0" = "$BASH_SOURCE" ]; then
  exit 0
else
  return 0
fi
