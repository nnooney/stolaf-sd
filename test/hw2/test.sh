#!/bin/bash

# This script is meant to be run from the main project folder, not the current
# folder it is in. Run it like this:
#  ./test/hw2/test.sh

TESTS=0
NUM_TESTS=14
ASSIGNMENT="hw2"

# Run the following tests, keeping track of their output. For each test, set
# the required variables, then execute the appropriate test.

# Inspect the source of types.cpp for several strings.
PROGRAM_FILE="types.cpp"
SEARCH_TEXT="char"
. test/utils/sourceInspect-test.sh
TESTS=$(($TESTS + $?))

PROGRAM_FILE="types.cpp"
SEARCH_TEXT="short"
. test/utils/sourceInspect-test.sh
TESTS=$(($TESTS + $?))

PROGRAM_FILE="types.cpp"
SEARCH_TEXT="int"
. test/utils/sourceInspect-test.sh
TESTS=$(($TESTS + $?))

PROGRAM_FILE="types.cpp"
SEARCH_TEXT="long"
. test/utils/sourceInspect-test.sh
TESTS=$(($TESTS + $?))

PROGRAM_FILE="types.cpp"
SEARCH_TEXT="bool"
. test/utils/sourceInspect-test.sh
TESTS=$(($TESTS + $?))

PROGRAM_FILE="types.cpp"
SEARCH_TEXT="float"
. test/utils/sourceInspect-test.sh
TESTS=$(($TESTS + $?))

PROGRAM_FILE="types.cpp"
SEARCH_TEXT="double"
. test/utils/sourceInspect-test.sh
TESTS=$(($TESTS + $?))

PROGRAM_FILE="types.cpp"
SEARCH_TEXT="long double"
. test/utils/sourceInspect-test.sh
TESTS=$(($TESTS + $?))

PROGRAM_FILE="types.cpp"
SEARCH_TEXT="wchar_t"
. test/utils/sourceInspect-test.sh
TESTS=$(($TESTS + $?))

PROGRAM_FILE="types.cpp"
SEARCH_TEXT="unsigned char"
. test/utils/sourceInspect-test.sh
TESTS=$(($TESTS + $?))

PROGRAM_FILE="types.cpp"
SEARCH_TEXT="unsigned short"
. test/utils/sourceInspect-test.sh
TESTS=$(($TESTS + $?))

PROGRAM_FILE="types.cpp"
SEARCH_TEXT="unsigned int"
. test/utils/sourceInspect-test.sh
TESTS=$(($TESTS + $?))

PROGRAM_FILE="types.cpp"
SEARCH_TEXT="unsigned long"
. test/utils/sourceInspect-test.sh
TESTS=$(($TESTS + $?))

PROGRAM_FILE="types.cpp"
SEARCH_TEXT="unsigned wchar_t"
. test/utils/sourceInspect-test.sh
TESTS=$(($TESTS + $?))

# All testing is complete.
PASSED_TESTS=`expr ${NUM_TESTS} - ${TESTS}`
echo "Test Results: ${PASSED_TESTS}/${NUM_TESTS}"

if [ "$0" = "$BASH_SOURCE" ]; then
  exit $TESTS
else
  return $TESTS
fi
