#!/bin/bash

# This is a utility file, and is not meant to be executed directly. Instead,
# run the test.sh script located within a homework or lab directory.

# sourceInspect.sh - a simple test that searches the source code for a given
# string.
#   Defined Variables:
#     ASSIGNMENT: the name of the assignment folder to search for files in
#     PROGRAM_FILE: the name of the source file which serves as the test name
#     SEARCH_TEXT: the string to search for in the source code of the file
#   Return Value:
#     0 if the search text is found within the program file
#     1 otherwise

# Since this is a simple program, we will test it by running it and comparing
# its output with something we expect.

echo "Starting Test: ${PROGRAM_FILE}"
echo "  Inspecting Source of ${PROGRAM_FILE} for \"${SEARCH_TEXT}\""

OUTPUT=`grep "${SEARCH_TEXT}" ${ASSIGNMENT}/${PROGRAM_FILE}`
RETVAL=$?

# Ensure that grep finds the text within the program file
if [ $RETVAL -eq 0 ]; then
  echo "  \"${SEARCH_TEXT}\" found, OK"
else
  echo "  Could not find \"${SEARCH_TEXT}\", FAIL"
  return 1
fi

# All Cases Passed
echo "Test: ${PROGRAM_FILE} PASSED"
return 0
