#!/bin/bash

# This is a utility file, and is not meant to be executed directly. Instead,
# run the test.sh script located within a homework or lab directory.

# runCompareOutput.sh - a simple test that runs a program and compares its
# output to an expected variable.
#   Defined Variables:
#     PROGRAM_FILE: the name of the source file which serves as the test name
#     PROGRAM: the path to the executable from the project root directory
#     EXPECTED_OUTPUT: A string value of the expected output from the program
#   Return Value:
#     0 if the program runs and returns 0 AND the expected output matches the
#       output of the program
#     1 otherwise

# Since this is a simple program, we will test it by running it and comparing
# its output with something we expect.

echo "Starting Test: ${PROGRAM_FILE}"
echo "  Running ${PROGRAM}"

OUTPUT=`./${PROGRAM}`
RETVAL=$?

# Make sure a 0 status was returned from the program
if [ $RETVAL -eq 0 ]; then
  echo "  Retval is 0, OK"
else
  echo "  Retval is not 0, FAIL"
  return 1
fi

# Make sure the output matches what we expect
if [ "$OUTPUT" == "${EXPECTED_OUTPUT}" ]; then
  echo "  Output is correct, OK"
else
  echo "  Output is incorrect, FAIL"
  echo "    Expected: ${EXPECTED_OUTPUT}"
  echo "    Actual: $OUTPUT"
  return 1
fi

# All Cases Passed
echo "Test: ${PROGRAM_FILE} PASSED"
return 0
