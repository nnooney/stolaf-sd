#!/bin/bash

# This script is meant to be run from the main project folder, not the current
# folder it is in. Run it like this:
#  ./test/runAll.sh

# This script will run all of the tests specified, similar to how the continuous
# integration script will automatically grade your tests when you push your code
# to stogit. To enable or disable which assignments get graded in this pipeline,
# comment or uncomment the section beneath each assignment section below.

# The function buildAndRun will build each assignment and run its tests.
buildAndRun() {
  local assignment
  local retval

  assignment=$1

  printf "[${assignment}] Building ... "
  . ./test/${assignment}/build.sh >/dev/null 2>&1
  retval=$?
  if [ $retval -ne 0 ]; then
    printf "FAIL\n"
    echo "Error building ${assignment}"
    echo "Debug this error by running \`./test/${assignment}/build.sh\`"
    return "$retval"
  fi
  printf "OK\n"

  printf "[${assignment}] Testing ... "
  . ./test/${assignment}/test.sh >/dev/null 2>&1
  retval=$?
  if [ $retval -ne 0 ]; then
    printf "FAIL\n"
    echo "Error during testing of ${assignment}"
    echo "Debug this error by running \`./test/${assignment}/test.sh\`"
    return "$retval"
  fi
  printf "OK\n"

  return 0
}

# To enable/disable testing of an assignment, comment the line beneath the
# assignment name.

# hw1
buildAndRun "hw1"

# hw2
buildAndRun "hw2"
