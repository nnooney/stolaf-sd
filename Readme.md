[![pipeline status][1]][2]

[1]: https://gitlab.com/nnooney/stolaf-sd/badges/master/pipeline.svg
[2]: https://gitlab.com/nnooney/stolaf-sd/pipelines/master/latest

# St. Olaf Software Design

Welcome to Software Design! These folders are where you will complete all
of the homework and labs for the course. Inside the _test_ directory are several
files that will automatically test the code that you submit. Please do not
modify any of these files, as they will interfere with the grading process.

## How to run tests

This repository is configured to automatically run and test your code whenever
you upload it to the server using `git push`. First, it looks in the main
project folder for a file called _.gitlab-ci.yml_. This file tells the server
how to build the project and what tests to run on it. The building and testing
are run through files called _build.sh_ and _test.sh_. There is one build file
and one test file for each homework and lab. These files are located within the
_test_ directory, along with files for each of the individual tests.

### Running tests when committing

All of the build and test scripts come included in this repository, located in
the _test_ directory. In order to tell the server to test your homework, a small
section needs to be added to the _.gitlab-ci.yml_ file. It looks like this:

```
hw1:
  stage: grading
  script:
    - ./test/hw1/build.sh
    - ./test/hw1/test.sh
```

The `hw1:` part makes a job for the server to run, and the `stage: grading`
indicates that this job is for grading (note that _grading_ is currently the
only supported stage the server recognizes). The `script:` section is a list of
commands to run for building and testing all of the files for the homework or
lab. You can replace `hw1` with the folder of the homework or lab you are
completing.

### Running tests locally

Because of this setup, it is possible to run tests locally. In order to run the
tests, change to the main project folder, and run the following commands:

```
./test/<hwX>/build.sh
./test/<hwX>/test.sh
```

Replace `<hwX>` with the folder of the lab or homework that you want to build
and test. All of the binaries and object files will be located in the _build_
directory in a folder matching the homework or lab you chose to build. Observe
that these are the same commands that the server runs to test the programs you
write, so that if the tests pass locally, you can be confident that the tests
will pass when you commit your code.
