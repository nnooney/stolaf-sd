#include <iostream>
using namespace std;

int main()
{
  char c_variable;
  unsigned char uc_variable;

  wchar_t wc_variable;
  unsigned wchar_t uwc_variable;

  short s_variable;
  unsigned short us_variable;

  int i_variable;
  unsigned int ui_variable;

  long l_variable;
  unsigned long ul_variable;

  bool b_variable;

  float f_variable;

  double d_variable;

  long double ld_variable;

  return 0;
}
