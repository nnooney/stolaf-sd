# HW1: Linux, emacs, and grammar

In this folder, you will place all of the source code you create for homework 1.
You can get instant feedback on the progress of your homework by running the
tests associated with this homework.

To do this, navigate to the root directory of this repository and run the
following commands:

```
./test/hw1/build.sh
./test/hw1/test.sh
```

The first command will compile your source code and place it in a `build` folder
in this repository. If the folder doesn't exist, then the script will
automatically make it.

The second command will run automated tests on both your source files and your
compiled programs. It will make sure that your program functions according to
the specification provided by the assignment.

## Files included in this homework

- Dog.cpp
- Dog.h
- first.cpp
- hello.cpp
- Makefile
- second.cpp
- secondComment.cpp
- tryDog.cpp
